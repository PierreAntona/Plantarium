<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/PierreAntona/Plantarium">
    <img src="./public/img/logo_light.png" alt="Logo" width="216" height="91">
  </a>

  <p align="center">
    Create the 2D plan of your future biodiversity reserve with ease.
    <br />
    In association with <a href="https://www.vanaprincipia.fr/"><strong>Vana Principia </strong></a>.
    <br />
    <br />    
    <a href="https://github.com/dalinda28">Dalinda Aloui</a>
    ·
    <a href="https://github.com/PierreAntona">Pierre Antona</a>
    ·
    <a href="https://github.com/souq77">Rabie Atta</a>
    ·
    <a href="https://github.com/Redak45">Ali Said Mohamadou</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

![Plantarium screenshot][plantarium-screenshot]

Plantarium allows users to create a garden true to their own, in two dimensions. They have the possibility of placing the plant species they want there while obtaining useful information on them.

### Built With

We use the React framework Next.js with the relational databases tool Prisma.

- [![Next][next.js]][next-url]
- [![React][react.js]][react-url]
- [![Prisma][prisma.io]][prisma-url]
- [![Mapbox][mapbox.com]][mapbox-url]
- [![Sass][sass.com]][sass-url]
- [![Axios][axios.com]][axios-url]

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

- npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```bash
   git clone https://gitlab.com/PierreAntona/Plantarium.git
   ```
2. Install NPM packages
   ```bash
   cd Plantarium
   npm install
   ```
3. Configure your database in the `.env` file
   ```bash
   DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"
   ```
4. Run your first migration
   ```bash
   npx prisma migrate dev --name init
   ```
5. Run the development server (http://localhost:3000)
   ```bash
   npm run dev
   ```

<!-- ROADMAP -->

## Roadmap

- [x] Login and Authentication
- [x] Garden creation
- [x] Boxes in garden
- [x] Elements in boxes
- [ ] Improve element management
  - [ ] Bind to a global database
  - [ ] Smart suggestions
- [ ] Improve boxes creation
  - [ ] Add different shapes
  - [ ] Add different types
- [ ] Responsive

See the [open issues](https://gitlab.com/PierreAntona/Plantarium/-/issues) for a full list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<!-- MARKDOWN LINKS & IMAGES -->

[plantarium-screenshot]: ./public/img/screenshot.png
[next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[next-url]: https://nextjs.org/
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DFB
[react-url]: https://reactjs.org/
[prisma.io]: https://img.shields.io/badge/Prisma-5167D8?style=for-the-badge&logo=prisma&logoColor=white
[prisma-url]: https://prisma.io
[mapbox.com]: https://img.shields.io/badge/Mapbox-000000?style=for-the-badge&logo=mapbox&logoColor=white
[mapbox-url]: https://www.mapbox.com/
[sass.com]: https://img.shields.io/badge/Sass-CC6699?style=for-the-badge&logo=sass&logoColor=white
[sass-url]: https://sass-lang.com/
[axios.com]: https://img.shields.io/badge/Axios-5A29E4?style=for-the-badge&logo=axios&logoColor=white
[axios-url]: https://axios-http.com/
