/*
  Warnings:

  - You are about to drop the column `parcellId` on the `element` table. All the data in the column will be lost.
  - You are about to drop the `elementInParcel` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `parcel` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `boxId` to the `element` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `element` DROP COLUMN `parcellId`,
    ADD COLUMN `boxId` INTEGER NOT NULL;

-- DropTable
DROP TABLE `elementInParcel`;

-- DropTable
DROP TABLE `parcel`;

-- CreateTable
CREATE TABLE `box` (
    `boxId` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `x` FLOAT NOT NULL,
    `y` FLOAT NOT NULL,
    `w` FLOAT NOT NULL,
    `h` FLOAT NOT NULL,

    PRIMARY KEY (`boxId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `elementInBox` (
    `elementInBoxId` INTEGER NOT NULL AUTO_INCREMENT,
    `boxId` INTEGER NOT NULL,
    `elementId` INTEGER NOT NULL,
    `qte` INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (`elementInBoxId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
