/*
  Warnings:

  - You are about to drop the column `elementId` on the `parcel` table. All the data in the column will be lost.
  - You are about to drop the column `qte` on the `parcel` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `parcel` DROP COLUMN `elementId`,
    DROP COLUMN `qte`;

-- CreateTable
CREATE TABLE `elementInParcel` (
    `elementInParcelId` INTEGER NOT NULL AUTO_INCREMENT,
    `parcelId` INTEGER NOT NULL,
    `elementId` INTEGER NOT NULL,
    `qte` INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (`elementInParcelId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
