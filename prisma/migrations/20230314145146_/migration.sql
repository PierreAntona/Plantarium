/*
  Warnings:

  - You are about to alter the column `gardenSize` on the `users` table. The data in that column could be lost. The data in that column will be cast from `Float` to `VarChar(100)`.

*/
-- AlterTable
ALTER TABLE `users` MODIFY `gardenSize` VARCHAR(100) NOT NULL;
