-- AlterTable
ALTER TABLE `users` MODIFY `garden` VARCHAR(1000) NOT NULL;

-- CreateTable
CREATE TABLE `parcel` (
    `parcelId` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `elementId` INTEGER NOT NULL,
    `coordonnees` VARCHAR(500) NOT NULL,
    `qte` INTEGER NOT NULL,

    PRIMARY KEY (`parcelId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `element` (
    `elementlId` INTEGER NOT NULL AUTO_INCREMENT,
    `parcellId` INTEGER NOT NULL,
    `name` VARCHAR(500) NOT NULL,
    `author` VARCHAR(500) NOT NULL,
    `familia` VARCHAR(500) NOT NULL,
    `lifeform` VARCHAR(500) NOT NULL,
    `habitus` VARCHAR(500) NOT NULL,
    `plantuse` VARCHAR(500) NOT NULL,
    `height` VARCHAR(500) NOT NULL,
    `exposure` VARCHAR(500) NOT NULL,
    `moisture` VARCHAR(500) NOT NULL,
    `soil` VARCHAR(500) NOT NULL,
    `hardiness` VARCHAR(500) NOT NULL,
    `leafshape` VARCHAR(500) NOT NULL,
    `leafdivision` VARCHAR(500) NOT NULL,
    `leafarrangement` VARCHAR(500) NOT NULL,
    `leafretention` VARCHAR(500) NOT NULL,
    `autumncolour` VARCHAR(500) NOT NULL,
    `floweringperiod` VARCHAR(500) NOT NULL,
    `flowercolour` VARCHAR(500) NOT NULL,
    `flowershape` VARCHAR(500) NOT NULL,
    `inflorescence` VARCHAR(500) NOT NULL,
    `fruit` VARCHAR(500) NOT NULL,
    `ordo` VARCHAR(500) NOT NULL,
    `superordo` VARCHAR(500) NOT NULL,
    `subclassis` VARCHAR(500) NOT NULL,
    `classis` VARCHAR(500) NOT NULL,
    `subdivisio` VARCHAR(500) NOT NULL,
    `divisio` VARCHAR(500) NOT NULL,

    PRIMARY KEY (`elementlId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `parametre` (
    `parametrelId` INTEGER NOT NULL AUTO_INCREMENT,
    `parametreNumber` INTEGER NOT NULL,
    `type` VARCHAR(500) NOT NULL,
    `meaning` VARCHAR(500) NOT NULL,

    PRIMARY KEY (`parametrelId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
