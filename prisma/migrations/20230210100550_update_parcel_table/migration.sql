/*
  Warnings:

  - You are about to alter the column `h` on the `parcel` table. The data in that column could be lost. The data in that column will be cast from `VarChar(250)` to `Float`.
  - You are about to alter the column `w` on the `parcel` table. The data in that column could be lost. The data in that column will be cast from `VarChar(250)` to `Float`.
  - You are about to alter the column `x` on the `parcel` table. The data in that column could be lost. The data in that column will be cast from `VarChar(250)` to `Float`.
  - You are about to alter the column `y` on the `parcel` table. The data in that column could be lost. The data in that column will be cast from `VarChar(250)` to `Float`.

*/
-- AlterTable
ALTER TABLE `parcel` MODIFY `h` FLOAT NOT NULL,
    MODIFY `w` FLOAT NOT NULL,
    MODIFY `x` FLOAT NOT NULL,
    MODIFY `y` FLOAT NOT NULL;
