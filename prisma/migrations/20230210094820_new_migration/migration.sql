/*
  Warnings:

  - You are about to drop the column `coordonnees` on the `parcel` table. All the data in the column will be lost.
  - Added the required column `h` to the `parcel` table without a default value. This is not possible if the table is not empty.
  - Added the required column `parcelName` to the `parcel` table without a default value. This is not possible if the table is not empty.
  - Added the required column `w` to the `parcel` table without a default value. This is not possible if the table is not empty.
  - Added the required column `x` to the `parcel` table without a default value. This is not possible if the table is not empty.
  - Added the required column `y` to the `parcel` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `parcel` DROP COLUMN `coordonnees`,
    ADD COLUMN `h` INTEGER NOT NULL,
    ADD COLUMN `parcelName` VARCHAR(500) NOT NULL,
    ADD COLUMN `w` INTEGER NOT NULL,
    ADD COLUMN `x` INTEGER NOT NULL,
    ADD COLUMN `y` INTEGER NOT NULL;
