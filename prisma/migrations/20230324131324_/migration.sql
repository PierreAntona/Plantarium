/*
  Warnings:

  - You are about to drop the `element` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `parametre` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `element`;

-- DropTable
DROP TABLE `parametre`;
