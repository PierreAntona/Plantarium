import { useSession } from "next-auth/react";
import css from "./index.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";

export default function Wrapper(props) {
  const session = useSession();
  const router = useRouter();

  if (
    (session !== null && session?.status === "authenticated") ||
    router.pathname === "/" ||
    router.query.page !== undefined
  ) {
    return props.children;
  } else {
    return (
      <div className={css.container}>
        <h1 className={css.title1}>Oups !!</h1>
        <h2 className={css.title2}>Vous n'avez pas accès à cette page ! </h2>
        <div className={css.link}>
          <Link href="/">Retour à l'accueil</Link>
        </div>
      </div>
    );
  }
}
