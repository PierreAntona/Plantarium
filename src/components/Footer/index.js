import css from "./index.module.scss";

export default function Footer() {
  return (
    <div className={css.container}>
      <span>
        <a target="_blank" href="https://gitlab.com/PierreAntona/Plantarium">
          GitLab du projet
        </a>
      </span>
    </div>
  );
}
