import React, { useEffect, useState } from "react";
import CenterAdmin from "./centerAdmin";
import css from "./index.module.scss";
import LeftAdmin from "./LeftAdmin";
import RightAdmin from "./RightAdmin";
import dataElements from "./../../data/data.json";
import axios from "axios";
import useSWR from "swr";

function GardenAdmin({ boxes, reload, gardenSize, session }) {
  const [createBox, setCreateBox] = useState(false);
  const [selectedBox, setSelectedBox] = useState(null);

  useEffect(() => {
    !selectedBox && boxes?.length > 0 && setSelectedBox(boxes[0].boxId);
  }, []);

  const allElements = dataElements.element;

  const fetcher = (url) =>
    axios.get(url, session?.user?.userId).then((res) => res.data);

  const { data, error, isLoading, mutate } = useSWR(
    "/api/getElementsInBox",
    fetcher
  );
  if (error) return <div>Failed to load</div>;
  if (isLoading) return <div>Loading...</div>;

  return (
    <div className={css.container}>
      <LeftAdmin
        selectedBox={selectedBox}
        setSelectedBox={setSelectedBox}
        boxes={boxes}
        elements={data}
        allElements={allElements}
      />
      <CenterAdmin
        createBox={createBox}
        setCreateBox={setCreateBox}
        selectedBox={selectedBox}
        setSelectedBox={setSelectedBox}
        boxes={boxes}
        allElements={allElements}
        elements={data}
        reload={reload}
        gardenSize={gardenSize}
      />
      <RightAdmin
        selectedBox={selectedBox}
        setSelectedBox={setSelectedBox}
        elements={data}
        allElements={allElements}
        reload={reload}
        reloadElements={() => mutate()}
        session={session}
      />
    </div>
  );
}

export default GardenAdmin;
