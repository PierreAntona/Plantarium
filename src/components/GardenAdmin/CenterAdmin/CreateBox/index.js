import { useSession } from "next-auth/react";
import React, { useEffect, useRef, useState } from "react";
import css from "./index.module.scss";

export default function CreateBox({
  gardenWidth,
  gardenHeight,
  setData,
  data
}) {
  const canvasRef = useRef(null);
  const ctxRef = useRef(null);

  const [isDrawing, setIsDrawing] = useState(false);

  const canvasOffsetX = useRef(null);
  const canvasOffsetY = useRef(null);
  const startX = useRef(null);
  const startY = useRef(null);

  const session = useSession();

  useEffect(() => {
    const canvas = canvasRef.current;

    canvas.width = gardenWidth;
    canvas.height = gardenHeight;
    canvas.style.width = `${gardenWidth}px`;
    canvas.style.height = `${gardenHeight}px`;

    const ctx = canvas.getContext("2d");
    ctx.lineCap = "round";
    ctx.strokeStyle = "#FFF";
    ctx.fillStyle = "rgba(255, 255, 255, 0.25";
    ctx.lineWidth = 3;
    ctxRef.current = ctx;

    const canvasOffset = canvas.getBoundingClientRect();
    canvasOffsetX.current = canvasOffset.left;
    canvasOffsetY.current = canvasOffset.top;
  }, []);

  const startDraw = ({ nativeEvent }) => {
    nativeEvent.preventDefault();
    nativeEvent.stopPropagation();

    startX.current = nativeEvent.clientX - canvasOffsetX.current;
    startY.current = nativeEvent.clientY - canvasOffsetY.current;

    setIsDrawing(true);
  };

  const stopDraw = () => {
    setIsDrawing(false);
    setData({
      ...data,
      userId: session.data.user.userId,
      x: startX.current,
      y: startY.current,
    });
  };

  const draw = ({ nativeEvent }) => {
    if (!isDrawing) {
      return;
    }

    nativeEvent.preventDefault();
    nativeEvent.stopPropagation();

    const newMouseX = nativeEvent.clientX - canvasOffsetX.current;
    const newMouseY = nativeEvent.clientY - canvasOffsetY.current;

    const rectWidth = newMouseX - startX.current;
    const rectHeight = newMouseY - startY.current;

    setData({
      ...data,
      w: rectWidth,
      h: rectHeight
    })

    ctxRef.current.clearRect(
      0,
      0,
      canvasRef.current.width,
      canvasRef.current.height
    );

    ctxRef.current.strokeRect(
      startX.current,
      startY.current,
      rectWidth,
      rectHeight
    );

    ctxRef.current.fillRect(
      startX.current,
      startY.current,
      rectWidth,
      rectHeight
    );
  };

  return (
    <canvas
      ref={canvasRef}
      className={css.gardenCanvas}
      onMouseDown={startDraw}
      onMouseMove={draw}
      onMouseUp={stopDraw}
      onMouseLeave={stopDraw}
    />
  );
}
