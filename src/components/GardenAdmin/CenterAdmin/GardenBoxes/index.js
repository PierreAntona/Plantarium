import React, { useEffect, useRef } from "react";
import css from "./index.module.scss";

export default function GardenBoxes({
  gardenWidth,
  gardenHeight,
  boxes,
  elements,
  allElements,
  setSelectedBox,
  selectedBox,
}) {
  const canvasRef = useRef(null);
  const canvasElementsRef = useRef(null);
  const ctxRef = useRef(null);

  let hover = false,
    id,
    _i,
    _b;

  useEffect(() => {
    if (gardenWidth != null) {
      const ctx = canvasRef.current.getContext("2d");
      canvasRef.current.width = gardenWidth;
      canvasRef.current.height = gardenHeight;
      canvasRef.current.style.width = `${gardenWidth}px`;
      canvasRef.current.style.height = `${gardenHeight}px`;

      const ctxElements = canvasElementsRef.current.getContext("2d");
      canvasElementsRef.current.width = gardenWidth;
      canvasElementsRef.current.height = gardenHeight;
      canvasElementsRef.current.style.width = `${gardenWidth}px`;
      canvasElementsRef.current.style.height = `${gardenHeight}px`;

      ctx.lineCap = "round";
      ctx.lineWidth = 3;
      ctxRef.current = ctx;

      renderBoxes(ctx);

      boxes.forEach((box) => {
        renderElements(box, ctxElements);
      });
    }
  }, [gardenWidth, boxes, elements]);

  function renderBoxes(ctx, hover) {
    for (_i = 0; (_b = boxes[_i]); _i++) {
      ctx.strokeStyle = hover && id === _i ? "#E9B752" : "#FFF";
      ctx.fillStyle =
        hover && id === _i
          ? "rgba(233, 183, 82, 0.25)"
          : "rgba(255, 255, 255, 0.25)";
      ctx.fillRect(_b.x, _b.y, _b.w, _b.h);
      ctx.strokeRect(_b.x, _b.y, _b.w, _b.h);
    }
  }

  function handleHover({ nativeEvent }) {
    const ctx = canvasRef.current.getContext("2d");
    let r = canvasElementsRef.current.getBoundingClientRect(),
      x = nativeEvent.clientX - r.left,
      y = nativeEvent.clientY - r.top,
      hover = false;

    ctx.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);

    for (let i = boxes.length - 1, b; (b = boxes[i]); i--) {
      if (b.w > 0 && b.h > 0) {
        if (x >= b.x && x <= b.x + b.w && y >= b.y && y <= b.y + b.h) {
          hover = true;
          id = i;
          break;
        }
      } else if (b.w < 0 && b.h < 0) {
        if (x <= b.x && x >= b.x + b.w && y <= b.y && y >= b.y + b.h) {
          hover = true;
          id = i;
          break;
        }
      } else if (b.w < 0 && b.h > 0) {
        if (x <= b.x && x >= b.x + b.w && y >= b.y && y <= b.y + b.h) {
          hover = true;
          id = i;
          break;
        }
      } else if (b.w > 0 && b.h < 0) {
        if (x >= b.x && x <= b.x + b.w && y <= b.y && y >= b.y + b.h) {
          hover = true;
          id = i;
          break;
        }
      }
    }

    renderBoxes(ctx, hover);
  }

  function handleSelection({ nativeEvent }) {
    let r = canvasRef.current.getBoundingClientRect(),
      x = nativeEvent.clientX - r.left,
      y = nativeEvent.clientY - r.top,
      selected;

    for (let i = boxes - 1, b; (b = boxes[i]); i--) {
      if (b.w > 0 && b.h > 0) {
        if (x >= b.x && x <= b.x + b.w && y >= b.y && y <= b.y + b.h) {
          selected = boxes[0].boxId;
          id = i;
          break;
        }
      } else if (b.w < 0 && b.h < 0) {
        if (x <= b.x && x >= b.x + b.w && y <= b.y && y >= b.y + b.h) {
          selected = boxes[0].boxId;
          id = i;
          break;
        }
      } else if (b.w < 0 && b.h > 0) {
        if (x <= b.x && x >= b.x + b.w && y >= b.y && y <= b.y + b.h) {
          selected = boxes[0].boxId;
          id = i;
          break;
        }
      } else if (b.w > 0 && b.h < 0) {
        if (x >= b.x && x <= b.x + b.w && y <= b.y && y >= b.y + b.h) {
          selected = boxes[0].boxId;
          id = i;
          break;
        }
      }
    }

    setSelectedBox(boxes[id]?.boxId);
  }

  function renderElements(box, ctx) {
    const middle = { x: box.x + box.w / 2, y: box.y + box.h / 2 };
    const dWidth = 30;
    const dHeight = 30;
    const dx = middle.x - dWidth / 2;
    const dy = middle.y - dHeight / 2;

    let c1 = 0;
    let c2 = 0;
    let marge = 15;

    elements.map((element) => {
      const img = new Image();

      if (box.boxId === element.boxId) {
        img.src = allElements[element.elementId].image;
        img.onload = () => {
          if (box.w > box.h) {
            if (c1 % 2 === 0) {
              if (box.x + box.w > dx + dWidth * c2 + marge * c2 + 20)
                ctx.drawImage(
                  img,
                  dx + dWidth * c2 + marge * c2,
                  dy,
                  dWidth,
                  dHeight
                );
            } else {
              c2++;
              if (box.x < dx - dWidth * c2 - marge * c2 - 20)
                ctx.drawImage(
                  img,
                  dx - dWidth * c2 - marge * c2,
                  dy,
                  dWidth,
                  dHeight
                );
            }
          }

          if (box.w < box.h) {
            if (c1 % 2 === 0) {
              if (box.y + box.h > dy + dHeight * c2 + marge * c2 + 20)
                ctx.drawImage(
                  img,
                  dx,
                  dy + dHeight * c2 + marge * c2,
                  dWidth,
                  dHeight
                );
            } else {
              c2++;
              if (box.y < dy - dHeight * c2 - marge * c2 - 20)
                ctx.drawImage(
                  img,
                  dx,
                  dy - dHeight * c2 - marge * c2,
                  dWidth,
                  dHeight
                );
            }
          }

          c1++;
        };
      }
    });
  }

  return (
    <>
      <canvas ref={canvasRef} className={css.gardenCanvas} />
      <canvas
        ref={canvasElementsRef}
        className={css.elementsCanvas}
        onMouseMove={handleHover}
        onClick={handleSelection}
      />
    </>
  );
}
