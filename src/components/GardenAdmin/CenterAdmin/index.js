import React, { useState, useRef, useEffect } from "react";
import { useSession } from "next-auth/react";
import css from "./index.module.scss";
import CreateBox from "./CreateBox";
import GardenBoxes from "./GardenBoxes";
import axios from "axios";

export default function CenterAdmin({
  createBox,
  setCreateBox,
  selectedBox,
  setSelectedBox,
  boxes,
  elements,
  allElements,
  gardenSize,
  reload,
}) {
  const gardenRef = useRef(null);
  const session = useSession();
  const svgStr = session.data.user.garden;

  const [gardenWidth, setGardenWidth] = useState(null);
  const [gardenHeight, setGardenHeight] = useState(null);
  const [data, setData] = useState(null);

  useEffect(() => {
    if (gardenRef) {
      setGardenWidth(gardenRef.current.offsetWidth);
      setGardenHeight(gardenRef.current.offsetHeight);
    }
  }, [gardenRef]);

  async function boxCreation() {
    try {
      setCreateBox(!createBox);
      const res = await axios.post("/api/createBox", data);
      reload();
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className={css.container}>
      <div className={css.header}>
        <span>Superficie : {gardenSize} m²</span>
        {!createBox ? (
          <div className={css.tools}>
            <button onClick={() => setCreateBox(!createBox)}>
              Nouvelle parcelle <img src="/img/icon_build.svg" />
            </button>
          </div>
        ) : (
          <div className={css.toolsMenu}>
            <button onClick={() => setCreateBox(!createBox)}>Annuler</button>
            <button onClick={() => boxCreation()}>Ajouter</button>
          </div>
        )}
      </div>
      <div
        className={css.garden}
        ref={gardenRef}
        dangerouslySetInnerHTML={{ __html: svgStr }}
      />
      {createBox && (
        <CreateBox
          gardenWidth={gardenWidth}
          gardenHeight={gardenHeight}
          data={data}
          setData={setData}
        />
      )}
      <GardenBoxes
        elements={elements}
        allElements={allElements}
        boxes={boxes}
        gardenWidth={gardenWidth}
        gardenHeight={gardenHeight}
        selectedBox={selectedBox}
        setSelectedBox={setSelectedBox}
      />
    </div>
  );
}
