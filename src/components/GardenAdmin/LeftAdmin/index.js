import React, { useEffect, useRef, useState } from "react";
import css from "./index.module.scss";

function LeftAdmin({
  selectedBox,
  setSelectedBox,
  boxes,
  elements,
  allElements,
}) {
  useEffect(() => {
    const allDetails = document.querySelectorAll("details");
    console.log(allDetails);

    allDetails.forEach((details) => {
      if (details.id == selectedBox) {
        details.open = true;
      } else {
        details.open = false;
      }
    });
  }, [selectedBox, boxes]);

  return (
    <div className={css.container}>
      <div className={css.parcelles}>
        <span className={css.headerParcelles}>Parcelles</span>
        {boxes.map((box, index) => (
          <details key={box.boxId} id={box.boxId}>
            <summary onClick={() => setSelectedBox(box.boxId)}>
              <span>Parcelle {box.boxId}</span>
            </summary>
            <ul>
              {elements.map((element) => {
                if (box.boxId === element.boxId) {
                  return (
                    <li key={element.elementId}>
                      {allElements[element.elementId].name}
                    </li>
                  );
                }
              })}
            </ul>
          </details>
        ))}
      </div>
    </div>
  );
}

export default LeftAdmin;
