import { Swiper, SwiperSlide } from "swiper/react";
import css from "./index.module.scss";
import "swiper/css";
import { useEffect, useState } from "react";
import axios from "axios";
import { useSession } from "next-auth/react";

function RightAdmin({
  selectedBox,
  setSelectedBox,
  elements,
  allElements,
  reload,
  reloadElements,
}) {
  const [currentElements, setCurrentElements] = useState([]);
  const [currentId, setCurrentId] = useState();
  const [activeElement, setActiveElement] = useState(0);

  const [openModal, setOpenModal] = useState(false);

  const session = useSession();
  const elementList = [];

  useEffect(() => {
    setCurrentElements(
      elements.filter((element) => element.boxId === selectedBox)
    );
  }, [selectedBox, elements]);

  const updateActiveElement = (swiperInstance) => {
    if (swiperInstance === null) return;
    const slides = swiperInstance?.slides;
    const currentSlide = swiperInstance?.activeIndex;

    slides.forEach((slide) => {
      slide.style.filter = "opacity(0.5)";
    });
    slides.forEach((slide) => {
      slide.firstChild.style.transform = "scale(0.65)";
    });

    if (slides[currentSlide]) {
      slides[currentSlide].firstChild.style.transform = "scale(1)";
      slides[currentSlide].style.filter = "none";
    }

    setActiveElement(currentSlide);
    setCurrentId(currentElements[currentSlide].elementId);
  };

  const initSwiper = (swiperInstance) => {};

  const deleteBox = async () => {
    const data = {
      boxId: selectedBox,
    };

    try {
      const res = await axios.post("/api/deleteBox", data);
      reload();
      setSelectedBox(null);
    } catch (err) {
      console.log(err);
    }
  };

  const addElement = async (id) => {
    const data = {
      userId: session.data.user.userId,
      boxId: selectedBox,
      elementId: parseInt(id),
    };
    
    try {
      const res = await axios.post("/api/addElement", data);
      setOpenModal(!openModal);
      reloadElements();
    } catch (err) {
      console.log(err);
    }
  };

  const Modal = () => {
    return (
      <div className={css.modalContainer}>
        <div className={css.modal}>
          <h2>Parcelle {selectedBox}</h2>
          <hr />
          <h3>Éléments disponibles :</h3>
          <ul>
            {Object.entries(allElements).map(([key, el]) => (
              <li key={el.name}>
                {el.name}
                <span className={css.element} onClick={() => addElement(key)}>
                  Ajouter
                </span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  };

  return (
    <div className={css.container}>
      {openModal && <Modal />}
      <div className={css.topPart}>
        <span>Parcelle {selectedBox}</span>{" "}
        <img onClick={() => deleteBox()} src="/img/icon_trash.png" />
        <div className={css.boxDetails}>
          <p className={css.label}>Éléments : {currentElements.length}</p>
          <p className={css.openModal} onClick={() => setOpenModal(!openModal)}>
            Modifier
          </p>
        </div>
      </div>
      <div className={css.bottomPart}>
        <span className={css.headerBottomPart}>Éléments</span>
        <div className={css.selector}>
          <Swiper
            slidesPerView={1}
            spaceBetween={2}
            centeredSlides={true}
            pagination={{ clickable: true }}
            initialSlide={activeElement}
            onActiveIndexChange={updateActiveElement}
            onAfterInit={initSwiper}
          >
            {currentElements.map((el) => {
              if (el.boxId === selectedBox)
                return (
                  <SwiperSlide
                    key={el.elementId}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <img src={allElements[el.elementId].image} />
                  </SwiperSlide>
                );
            })}
          </Swiper>
        </div>
        <div className={css.details}>
          <div className={css.name}>
            {currentId && allElements[currentId].name}
            {/* <div className={css.elementsGestion}>
              <span>+</span>
              <span>-</span>
            </div> */}
          </div>
          <p className={css.label}>Description :</p>
          <p>{currentId && allElements[currentId].desc}</p>
          {/* <p className={css.label}>Besoins :</p>
          <p className={css.label}>Croissance :</p>
          <p className={css.label}>Nuisibles :</p>
          <p className={css.label}>Faiblesse :</p> */}
        </div>
      </div>
    </div>
  );
}

export default RightAdmin;
