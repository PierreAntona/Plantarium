import { prisma } from "../../utils/prisma";

export default async (req, res) => {
  const { userId } = req.body;
  try {
    const boxes = await prisma.box.findMany({
      where: {
        userId: {
          equals: userId,
        },
      },
    });

    res.json(boxes);
  } catch (err) {
    res.json(err);
    res.status(405).end();
  }
};
