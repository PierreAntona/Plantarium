import { prisma } from "../../utils/prisma";

export default async (req, res) => {
  const { userId } = req.body;
  try {
    const elementsInBox = await prisma.elementInBox.findMany({
      where: {
        userId: {
          equals: userId,
        },
      },
    });

    res.json(elementsInBox);
  } catch (err) {
    res.json(err);
    res.status(405).end();
  }
};