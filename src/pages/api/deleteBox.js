import { prisma } from "../../utils/prisma";

export default async (req, res) => {
  const { boxId } = req.body;

  try {
    await prisma.box.delete({
      where: {
        boxId: boxId,
      },
    });

    return res.status(200).end();
  } catch (err) {
    return res.status(503).json({ err: err.toString() });
  }
};
