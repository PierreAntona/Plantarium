import { prisma } from "../../utils/prisma";

export default async (req, res) => {
  const { userId, boxId, elementId } = req.body;

  try {
    await prisma.elementInBox.create({
      data: {
        userId: userId,
        boxId: boxId,
        elementId: elementId,
      },
    });
    return res.status(200).end();
  } catch (err) {
    return res.status(503).json({ err: err.toString() });
  }
};
