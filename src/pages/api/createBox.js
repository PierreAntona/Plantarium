import { prisma } from "../../utils/prisma";

export default async (req, res) => {
  const { userId, x, y, w, h } = req.body;

  try {
    await prisma.box.create({
      data: {
        userId: userId,
        x: x,
        y: y,
        w: w,
        h: h
      },
    });
    return res.status(200).end();
  } catch (err) {
    return res.status(503).json({ err: err.toString() });
  }
};
