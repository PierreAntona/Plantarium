import React from "react";
import Head from "next/head";
import GardenAdmin from "../components/GardenAdmin";
import css from "../styles/Home.module.scss";
import Header from "../components/Header";
import { getSession, useSession } from "next-auth/react";
import useSWR from "swr";
import axios from "axios";

function Dashboard() {
  const session = getSession();
  const uSession = useSession();
  const fetcher = (url) =>
    axios.get(url, session?.user?.userId).then((res) => res.data);

  const { data, error, isLoading, mutate } = useSWR(
    "/api/getAllBoxes",
    fetcher
  );
  if (error) return <div>Failed to load</div>;
  if (isLoading) return <div>Loading...</div>;

  return (
    <div className={css.container}>
      <Head>
        <title>Plantarium</title>
        <meta name="description" content="Plantarium" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={[css.main, css.admin].join(" ")}>
        <Header isLog={true} />
        <GardenAdmin
          boxes={data.filter(
            (box) => box.userId == uSession?.data?.user?.userId
          )}
          reload={() => mutate()}
          gardenSize={uSession?.data?.user?.gardenSize}
          session={session}
        />
      </main>
    </div>
  );
}

export default Dashboard;
